During my internship in the R&D department of my school (2018), I developed a software that can create fault trees from an XMI file.

When a system like a smartphone or a TV have a breakdown, we need to know where the failure comes from to find a solution. Fault Trees are made for this.
From a system with components and connexions, you can create an XMI model (with Papyrus from example). 
This XMI file will contain all the interactions between the components (inputs, outputs, names ...).
Then we go through the XMI file and analyze the text that it contains. 
We extract information from this file, use graphs and networks to create the fault trees.
Finally, we print the fault trees to find the reason of the failure.

(send me an email at souverainc@eisti.eu if you want to know more about the code)